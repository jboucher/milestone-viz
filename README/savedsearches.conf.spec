[default]
display.visualizations.custom.milestone-viz.milestone.optimize = <boolean>
display.visualizations.custom.milestone-viz.milestone.aggregateBy = <string>
display.visualizations.custom.milestone-viz.milestone.orientation = <string>